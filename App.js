import * as React from 'react';
import * as Location from 'expo-location';
import MapView, { Marker, Callout, Polyline }from 'react-native-maps';
import { StyleSheet, View, Text, Alert, Modal, Pressable } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import MapViewDirections from 'react-native-maps-directions';

export default function App() {

  const [modalVisible, setModalVisible] = React.useState(false);

  const zoologico = {
    name: 'Parque Zoologico de Caricuao',
    latitude:10.433215018289262, 
    longitude:-66.95951753557851
  }
  const materno = {
    name: 'Materno Infantil de Caricuao',
    latitude:10.438225861261083, 
    longitude:-66.99117692036025
  }
  const [origin, setOrigin] = React.useState({
    name: 'Ubicacion Actual',
    latitude:10.434987,
    longitude:-66.985442
  })
  const [destination, setDestination] = React.useState({
    name: "Parque Zoologico de Caricuao",
    latitude:10.433215018289262,
    longitude:-66.95951753557851,
  })
  async function testDestination(item, itemIndex){
    const current = {
      name: item.name,
      latitude: item.latitude,
      longitude: item.longitude
    }
    console.log(current)
    setDestination(current);
  }
  React.useEffect(() => {
    const interval = setInterval(() => {
      getLocationPermission();

      console.log("Cargando Cada 10 Segundos...")
    }, 10000);
  },[])

  async function getLocationPermission(){
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
      alert('Permiso denegado');
      return;
    }
    let location = await Location.getCurrentPositionAsync({})
    const current = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude
    }
    console.log(current)
    setOrigin(current);

  }

  return (
    <View style={styles.container}>
    
    <View style={styles.centeredView}>
      <View style={{width:'50%', alignItems:'center', justifyContent:"center"}}>
        <Picker style={{width:'70%'}}
          selectedValue={destination}
          onValueChange={(itemValue, itemIndex) => testDestination(itemValue)
          }
          >
          <Picker.Item style={{color:"blue"}} label={'*'+destination.name} value={zoologico} />
          <Picker.Item label={materno.name} value={materno} />
          <Picker.Item label={zoologico.name} value={zoologico} />

        </Picker>
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            
          <MapView style={styles.map} 
            initialRegion={{
              latitude: origin.latitude,
              longitude: origin.longitude,
              latitudeDelta: 0.09,
              longitudeDelta: 0.04,
            }}
            >
            <Marker 
              coordinate={origin}
              pinColor="black"
              />
            <Marker 
              coordinate={destination}
              />
            <Polyline 
              coordinates={[origin, destination]}
              strokeColor="purple"
              strokeWidth={4}
              />
          </MapView>



            <View style={{display: "flex", flexDirection:"row"}}> 
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={styles.textStyle}>Hide Map</Text>
            </Pressable>

            <Pressable
              style={[styles.button, styles.buttonOpen]}
              onPress={() => getLocationPermission()}>
              <Text style={styles.textStyle}>Reload Map</Text>
            </Pressable>
            </View>  
            
          </View>
        </View>
      </Modal>

      
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}>
        <Text style={styles.textStyle}>Show Map</Text>
      </Pressable>
      
      
    </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: 400,
    height: 400,
  },
  centeredView: {
    flex: 3,
    width:'100%',
    height:'100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'gray',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    marginTop: 5,
    marginHorizontal: 5,
    backgroundColor: '#17A589',
  },
  buttonClose: {
    marginTop: 5,
    marginHorizontal: 5,
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  

});

