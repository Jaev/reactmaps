# reactMaps

## Fast Description
Descripcion de la Rama Principal (main).

Este es un ejemplo de una aplicación de mapa de React Native que utiliza la biblioteca de mapas React Native Maps. La aplicación permite al usuario seleccionar un destino desde una lista desplegable y muestra la ruta y la ubicación actual en un mapa. La aplicación utiliza la biblioteca Expo Location para obtener la ubicación actual del usuario y utiliza el estado de React para almacenar la ubicación actual y de destino. Además, la aplicación utiliza el componente Modal de React Native para mostrar y ocultar el mapa y los botones de actualización y ocultamiento del mapa.

Descripcion de la Rama Secundaria (sample-external-map).

Este código de React Native muestra cómo utilizar la biblioteca react-native-navigation-directions para abrir un mapa y mostrar la ruta entre dos puntos en un dispositivo móvil. El código establece dos puntos de partida y finalización aleatorios y un plan de transporte 'w' (a pie). Cuando se presiona el botón "Open map", se llama a la función _callShowDirections, que utiliza la biblioteca mencionada para abrir un mapa y mostrar la ruta entre los dos puntos en el dispositivo móvil del usuario.
